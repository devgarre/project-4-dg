﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnd : MonoBehaviour {

	public GameObject winScreen;
	public GameObject gameOver;
	public GameObject currentGame;
	public int lives;
	public Vector3 lastCheckpoint;
	public AudioClip playerHit;

	// Use this for initialization
	void Start () {
		lastCheckpoint = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		//sets reset location to player location on contact with checkpoint
		if (other.tag == "Checkpoint")
		{
			lastCheckpoint = transform.position;
			Destroy (other.gameObject);
		}
		if (other.tag == "Enemy") 
		{
			//resets player to last checkpoint and reduces lives by 1
			if (lives > 0) {
				AudioSource.PlayClipAtPoint (playerHit, transform.position);
				transform.position = lastCheckpoint;
				lives = lives - 1;
				Debug.Log (lives);
			}
			//if player is out of lives, sets gave over screen to active
			else {
				gameOver.SetActive (true);
				currentGame.SetActive (false);
			}
		}
		//sets victory screen active and gameplay inactive
		if (other.tag == "Objective") 
		{
			winScreen.SetActive (true);
			currentGame.SetActive (false);
		}
	}
}
