﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrab : MonoBehaviour {

	public static bool RedKeyGrab = false;
	public static bool YellowKeyGrab = false;
	public static bool GreenKeyGrab = false;
	public AudioClip pickUpSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Red Key") 
		{
			AudioSource.PlayClipAtPoint (pickUpSound, new Vector3 (-3, 2, 0));
			RedKeyGrab = true;
		}
		if (other.tag == "Yellow Key") 
		{
			AudioSource.PlayClipAtPoint (pickUpSound, new Vector3 (6, 7, 0));
			YellowKeyGrab = true;
		}
		if (other.tag == "Green Key") 
		{
			AudioSource.PlayClipAtPoint (pickUpSound, new Vector3 (0, 14, 0));
			GreenKeyGrab = true;
		}
	}
}
