﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardMovement : MonoBehaviour {

	public int guardSpeed;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		transform.position -= transform.right * guardSpeed * Time.deltaTime;
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.tag == "Waypoint 1") {
			transform.localRotation = Quaternion.Euler (0, 180, 0);
		}
		if (other.tag == "Waypoint 2") {
			transform.localRotation = Quaternion.Euler (0, 0, 0);
		}
	}
}
